<?php get_header(); ?>

<style>
.menu__item{overflow:hidden;padding:0; margin: .35% 0; height: 215px;}.menu__item__content{background-color:white;bottom:0;color:#444;height:61px;left:0;padding:1em;position:absolute;width:100%;-webkit-transition:all .2s ease-out;transition:all .2s ease-out}.menu__item__title{color:#313547;font-size:1.2rem;overflow:hidden;text-overflow:ellipsis;white-space:nowrap}@media only screen and (min-width: 64em){.menu__item__title{font-size:1.5rem}}.menu__item:hover .menu__item__content,.menu__item:focus .menu__item__content{height:100%}.link--grade{color:#E1E8ED;font-weight:bold}.link--grade--pre{color:#E2624F}.link--grade--e{color:#50afdf}.link--grade--ms{color:#FFAE3D}.link--grade--hs{color:#21AABD}
</style>

<div class="has-cards" id="content">

	<div class="wrap clearfix">

		<main id="main" class="clearfix hero--small wrap" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div class="col-md--sixcol col-lg--fourcol">

  				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
            <?php if ( has_post_thumbnail() ) : ?>
            <div class="card card--media card--shadow no-margin no-padding media" style="border-bottom: none;">
              <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail( 'full' ); ?>
              </a>
            </div>
  					<?php endif; ?>

            <div class="card card--shadow">
              <header class="card__header" style="margin-bottom: 1em;">
                <a class="link link--undecorated _link-blue" href="<?php the_permalink(); ?>">
                  <h2 class="menu__item__title no-margin"><?php the_title(); ?></h2>
                </a>
              </header>
              <section class="content no-margin">
                <?php the_excerpt(); ?>
              </section>
              <footer class="card__footer" style="border-top: 1px solid #ddd; padding-top: 1em;">
                <a href="<?php the_permalink(); ?>" class="link link--undecorated">Read more</a>
                <?php
                    printf(__('<time style="color: #999; float: right; top: 5px; position: relative;" class="updated small-text" datetime="%1$s" pubdate>%2$s</time>', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')) );
                ?>
              </header>
              </footer>
            </div>

  				</article> <!-- end article -->
        </div><!--/column-->
			<?php endwhile; ?>

			    <?php if (function_exists('bones_page_navi')) { // if expirimental feature is active ?>

			        <?php bones_page_navi(); // use the page navi function ?>

		        <?php } else { // if it is disabled, display regular wp prev & next links ?>
			        <nav class="wp-prev-next">
				        <ul class="clearfix">
					        <li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
					        <li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
				        </ul>
			        </nav>
		        <?php } ?>

		    <?php else : ?>

			    <article id="post-not-found" class="hentry clearfix">
			    	<header class="article-header">
			    		<h1><?php _e("Sorry, No Results.", "bonestheme"); ?></h1>
			    	</header>
			    	<section class="post-content">
			    		<p><?php _e("Try your search again.", "bonestheme"); ?></p>
			    	</section>
			    	<footer class="article-footer">
			    	    <p><?php _e("This is the error message in the search.php template.", "bonestheme"); ?></p>
			    	</footer>
			    </article>

		    <?php endif; ?>

	    </main> <!-- end #main -->

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
