<?php

wp_redirect( 'http://sherman.library.nova.edu/', 301 );


?>

<?php get_header( '2.0' ); ?>

<div class="has-cards" id="main">

	<main class="hero wrap clearfix" role="main">


		<section class="col-md--twelvecol col-lg--eightcol clearfix tabs js-overlay js-overlay--on-focus">

			<ul class="clearfix menu tab-nav">

				<li class="active menu--actions--public__menu-item no-margin" style="order:4;">
					<a href="#">Catalog</a>
				</li><!--

			--><li class="menu--actions--public__menu-item no-margin" style="order:3;">
					<a href="#">Full-Text Finder</a>
				</li><!--

			--><li class="menu--actions--public__menu-item no-margin" style="order:2;">
					<a href="#">Google Scholar</a>
				</li><!--

			--><li class="menu--actions--public__menu-item no-margin" style="order:1;">
					<a href="#">APA</a>
				</li>

			</ul><!--/.tab-nav-->

		  	<!--Advanced Search-->
		  	<section class="card tab-content active">

				<header>
					<h2 class="delta no-margin">NovaCat</h2>
					<p class="zeta">Search our catalog for books, media, and periodicals.</p>
				</header>

				  	<form class="form js-adv-search" action="/search/X" method="get" id="advanced-search" name="search">

						<ul class="no-margin">

							<li class="clearfix form__field js-adv-search__keyword no-margin">

								<div class="col-md--threecol">
									<div class="form__select form__input--full-width">
										<select name="fieldLimit">
											<option value="" selected="">Keyword</option>
											<option value="a:">Author</option>
											<option value="t:">Title</option>
											<option value="d:">Subject</option>
										</select>
									</div>
								</div>

								<div class="col-md--ninecol">
									<label class="hide-accessible">Search the Catalog</label>
									<input class="form__input form__input--full-width" name="searchText" type="search" required>
								</div>

							</li>

							<li class="clearfix form__field no-margin">

								<div class="col-md--sixcol">
									<input class="checkbox-toggle" type="checkbox" id="m" name="m" value="n" />
									<label class="button button--small small-text button--flat button--unselect" for="m" style="border: 1px solid #ddd; color: rgba( 68, 68, 68, 0.5);">Just <b>Ebooks</b></label>
								</div>

								<div class="col-md--sixcol align-right">
									<a href="http://novacat.nova.edu/search/X" class="button button--link small-text">Advanced Search</a>
									<button class="button button--primary button--small small-text" value="Search" type="submit">Search</button>
								</div>

							</li>

						</ul>

					</form>

			</section><!--/.tab-content-->

			<section class="card tab-content">
				<header>
					<h2 class="delta no-margin">Full-Text Finder</h2>
					<p class="zeta">Enter the name of a journal, newspaper, magazine, or periodical to discover which databases have access to it.</p>
				</header>

				<form action="http://atoz.ebsco.com/Titles/SearchResults/1507" class="form" id="searchForm" method="get" name="frmSimpleSearch" role="form">

					<input data-val="true" id="IsFromAdvancedSearch" name="IsFromAdvancedSearch" value="True" type="hidden">
					<input name="GetResourcesBy" id="TitleNameSearch" value="TitleNameSearch" type="hidden">

					<ul>

						<li class="clearfix form__field no-margin">
							<div class="col-md--threecol">
								<div class="form__select form__input--full-width">
									<select name="SearchType" id="SearchType">
										<option value="Contains" id="Contains" selected="selected">Contains</option>
										<option value="BeginsWith" id="BeginsWith">Begins With</option>
										<option value="ExactMatch" id="ExactMatch">Exact Match</option>
									</select>
								</div>
							</div>
							<div class="col-md--ninecol">
								<input class="form__input form__input--full-width" data-val="true"id="Find" name="Find" type="text">
							</div>
						</li>

						<div class="align-right">
							<input class="button button--small small-text button--primary" id="nsubtnSubmit1" value="Search" onclick="_gaq.push(['_trackEvent', 'Search Widget', 'Submit', 'Journal Finder Search' ]);" type="submit">
						</div>

					</ul>

				</form>
			</section>

			<section class="card tab-content">
				<header>
					<h2 class="delta no-margin">Google Scholar</h2>
					<p class="zeta">Prefer Google? It won't limit to peer-reviewed sources.</p>
				</header>

				<form action="//ezproxylocal.library.nova.edu/form?qurl=http%3a%2f%2fscholar.google.com%2fscholar" class="form" name="f" method="post" role="form">
					<ul>

						<li class="clearfix form__field no-margin">
							<label class="hide-accessible" for="q">Find select scholarly articles</label>
							<input class="form__input form__input--full-width" name="q" maxlength="256" type="text">
						</li>

						<input name="hl" value="en" type="hidden">
						<input name="inst" value="webbridge-NOVAL" type="hidden">

						<div class="align-right">
							<input value="Search" class="button button--small button--primary small-text" onclick="_gaq.push(['_trackEvent', 'Search Widget', 'Submit', 'Google Scholar Search' ]);" type="submit">
						</div>

						<input name="as_sfid" value="AAAAAAUbUHZd6G7_hFTf0dayJ1IjeClt4h_wSwbP59E_RWHtKu3Mz-wcBhMe7URffez0f0hEvSUxJxvL4SIL9OInC3TsMZzqZtcETpYzSeS1uIXPyw==" type="hidden"><input name="as_fid" value="nEjI6YIV8Nwf_3bOFgiB" type="hidden">
					</ul>

				</form>

				<footer>
					<p class="small-text no-margin"><b>Tip:</b> To see full-text availability use the "Find It @ NSU" button.</p>
				</footer>
			</section>

			<section class="card tab-content">

				<header>
					<h2 class="delta no-margin">APA</h2>
					<p class="zeta">
						Search our <a class="link link--undecorated" href="http://sherman.library.nova.edu/sites/apa/">citation app</a>
						to quickly find formatting examples. Just finish this sentence:
					</p>
				</header>

				<form action="//sherman.library.nova.edu/sites/apa/" class="form" role="form">
					<ul>
						<li class="clearfix form__field no-margin">
							<div class="col-md--threecol">
								<label class="form__label" for="s">How do I cite ...</label>
							</div>
							<div class="col-md--ninecol">
								<input type="text" class="form__input form__input--full-width input--transparent" name="s" placeholder="e.g., a podcast">
							</div>

						</li>
						<div class="align-right">
							<button class="button button--primary button--small small-text" type="submit">Search</button>
						</div>
					</ul>
				</form>

			</section><!--/.tab-content-->

		</section><!--/.tabs-->


		<div class="col-md--twelvecol col-lg--fourcol clearfix" ng-controller="Databases">

			<div class="col-lg--twelvecol">
				<section class="accordion" style="margin-bottom: 1em;">
					<section class="accordion__section" id="ask">
						<a class="link link--undecorated" href="#ask">
							<h2 class="accordion__section__title zeta" style="border-color: #069;">
								<svg class="svg" viewBox="0 0 32 32" style="width: 21px; float: right; margin-right: .5em; bottom: 5px; position: relative; fill: #069;">
									<use xlink:href="#icon-bubbles"></use>
								</svg>

								Ask a Librarian
							</h2>
						</a>

						<div class="accordion__section__content">
							<ul class="list list--alternate">
								<li><b>Reference desk</b> is open until 9 p.m. <br><small>(8 p.m. on Saturdays)</small>. <a class="link link--undecorated small-text" href="#">See hours.</a></li>
								<li><b>Call</b> <a class="link link--undecorated" tel="9542624613">(954) 262-4613</a> <small>(<a href="http://sherman.library.nova.edu/sites/contact/" class="align-right">Toll-Free</a>)</small></li>
								<li><b>Email</b> <a class="link link--undecorated" href="http://nova.edu/library/main/ask.html#by-email">refdesk@nova.edu</a></li>
								<li><b>Chat</b> with <a class="link link--undecorated" href="">a Librarian</a></li>
								<li><b>Appointment</b> request <a href="http://systems.library.nova.edu/form/view.php?id=22">form</a></li>
								<li><b>Text</b> (954) 372-3505 <br><small>Start your question with "NSU".</small></li>
							</ul>
						</div>
					</section>
				</section>

				<section class="accordion">

					<section class="accordion__section" id="databases-by-name">
						<a class="link link--undecorated" href="#databases-by-name">
							<h2 class="accordion__section__title zeta">Databases by Name</h2>
						</a>

						<div class="accordion__section__content">
							<input class="form__input form__input--full-width" placeholder="Start typing a database title" ng-model="filter"/>
							<ul class="list list--alternate">

								<li ng-repeat="database in databases | filter: filter:startsWith | orderBy: 'title'">
									<a href="//sherman.library.nova.edu/auth/index.php?aid={{database.db_id}}">{{database.title}}</a>
								</li>

							</ul>
						</div>

				</section>

				<section class="accordion" style="margin-bottom: 1em;">
					<section class="accordion__section" id="databases-by-subject">
						<a href="#databases-by-subject" class="link link--undecorated">
							<h2 class="accordion__section__title zeta">Databases by Subject</h2>
						</a>
						<div class="accordion__section__content">
							<input class="form__input form__input--full-width" placeholder="Start typing a subject area" ng-model="query"/>
							<ul class="list list--alternate">

								<li ng-repeat="subject in subjects | filter: query | orderBy: 'name'">
									<a href="//sherman.library.nova.edu/e-library/index.php?action=subject&cat={{subject.subj_id}}">{{subject.name}}</a>
								</li>

							</ul>
						</div>
					</section>
				</section>

				</section>
			</div>


		</div>

	</main>

</div>

<div class="clearfix hero wrap">

	<div class="col-md--twelvecol col-lg--eightcol">

			<div class="col-md--fourcol">
				<h3 class="delta no-margin">Borrow</h3>
				<ul class="zeta no-bullets">

					<li><a class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://nova.campusguides.com/c.php?g=112162&p=724231', 'Course Reserves']);" href="http://nova.campusguides.com/c.php?g=112162&p=724231">Course Reserves</a></li>
					<li><a class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://sherman.library.nova.edu/sites/services/docdel/', 'Interlibrary Loan']);" href="http://sherman.library.nova.edu/sites/services/docdel/">Interlibrary Loan</a></li>
					<li><a class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://sherman.library.nova.edu/sites/policies/circulation/#borrowing', 'Loan Periods']);" href="http://sherman.library.nova.edu/sites/policies/circulation/#borrowing">Loan Periods</a></li>
					<li><a class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://sherman.library.nova.edu/new/', 'New Materials']);" href="http://sherman.library.nova.edu/new/">New Materials</a></li>
					<li><a class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://sherman.library.nova.edu/rooms', 'Study Rooms']);" href="http://sherman.library.nova.edu/rooms">Study Rooms</a></li>
				</ul>
			</div>

			<div class="col-md--fourcol">
				<h3 class="delta no-margin">Research</h3>
				<ul class="zeta no-bullets">
					<li><a class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://nova.campusguides.com/nsu-alumni', 'Alumni']);" href="http://nova.campusguides.com/nsu-alumni">Alumni</a></li>
					<li><a class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://copyright.nova.edu', 'Copyright']);" href="http://copyright.nova.edu">Copyright</a></li>
					<li><a class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://nova.campusguides.com/aslfaculty', 'Faculty']);" href="http://nova.campusguides.com/aslfaculty">Faculty</a></li>
					<li><a class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://www.nova.edu/library/', 'NSU Libraries']);" href="http://www.nova.edu/library/">NSU Libraries</a></li>
					<li><a class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://nsuworks.nova.edu', 'NSUWorks']);" href="http://nsuworks.nova.edu">NSUWorks</a></li>
				</ul>
			</div>


			<div class="col-md--fourcol">
				<h3 class="delta no-margin">Help</h3>
				<ul class="zeta no-bullets">
					<li><a class="link link--undecorated"  onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://sherman.library.nova.edu/sites/contact', 'Contact Us']);" href="#feedback">Contact Us</a></li>
					<li>
						<a  class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://sherman.library.nova.edu/sites/learn', 'LibraryLearn']);" href="http://sherman.library.nova.edu/sites/learn">LibraryLearn Videos</a> <br>
					</li>
					<li><a  class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://systems.library.nova.edu/form/view.php?id=22', 'Make an Appointment']);" href="http://systems.library.nova.edu/form/view.php?id=22">Make an Appointment</a></li>
					<li><a  class="link link--undecorated" onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://nova.campusguides.com/razors-research-bytes', 'Razors Research Bites']);" href="http://nova.campusguides.com/razors-research-bytes">Razor's Research Bites</a></li>
					<li><a class="link link--undecorated"  onclick="_gaq.push(['_trackEvent', 'Homepage Quick Links', 'http://sherman.library.nova.edu/sites/workshops-and-instruction', 'Workshops']);" href="http://sherman.library.nova.edu/sites/workshops-and-instruction">Workshops</a></li>

				</ul>
			</div>

		</div>

		<div class="col-lg--fourcol" ng-controller="Guides">


	    <h3 class="delta" style="margin-bottom: .5em;">Library Guides <a class="small-text" href="#libguides">(What's this?)</a></h3>

	    <input id="guide-search" class="awesomplete form__input form__input--full-width" placeholder="Subject Guides" ng-model="narrow"/>
	    <ul class="list list--alternate list--datalist" id="mylist">
	    	<li ng-repeat="guide in guides | filter: narrow | orderBy: 'name'">
	    		<a href="//nova.campusguides.com/sb.php?subject_id={{ guide.id }}">
	    			{{ guide.name }}
	    		</a>
	    	</li>
	    </ul>

		</div>

</div>

<div class="has-cards clearfix" ng-controller="Events">

	<div class="wrap hero clearfix">

		<div class="col-md--fourcol">

			<article class="card card--excerpt">
				<div class="card__media">
					<a href="http://sherman.library.nova.edu/sites/spotlight/event/portrait-warrior-conversation-artist/" onClick="_gaq.push(['_trackEvent', 'Features - Home Page', 'Click', 'Conversation with the Artist']);">
						<img data-src="http://public.library.nova.edu/wp-content/uploads/2015/11/portrait-of-a-warrior.jpg">
					</a>
				</div>

				<header class="card__header">
					<a href="http://sherman.library.nova.edu/sites/spotlight/event/portrait-warrior-conversation-artist/" onClick="_gaq.push(['_trackEvent', 'Features - Home Page', 'Click', 'Conversation with the Artist']);" class="link link--undecorated _link-blue">
						<h2 class="menu__item__title no-margin">Conversation with the Artist</h2>
						<p class="small-text">Portrait of a Warrior Exhibit</p>
					</a>
				</header>

				<section class="content no-margin">
					<p>
						Meet the artist who will discuss her incredible experience painting war veterans.
					</p>
				</section>

				<footer class="clearfix">

					<div class="col-md--sixcol share" style="margin-top: 0;">
						<a class="link" onClick="_gaq.push(['_trackEvent', 'Shares', 'Click', 'Facebook']);" href="https://www.facebook.com/sharer/sharer.php?u=http://sherman.library.nova.edu/sites/spotlight/event/portrait-warrior-conversation-artist/" title="Share on Facebook" target="new"><svg class="svg svg--facebook"><use xlink:href="#icon-facebook"></use></svg></a>
						<a class="link" onClick="_gaq.push(['_trackEvent', 'Shares', 'Click', 'Twitter']);" href="https://twitter.com/intent/tweet?text=http://sherman.library.nova.edu/sites/spotlight/event/portrait-warrior-conversation-artist/" title="Share on Twitter" target="new">
							<svg class="svg svg--twitter"><use xlink:href="#icon-twitter"></use></svg>
						</a>
					</div>

					<div class="col-md--sixcol align-right">
						<a href="http://sherman.library.nova.edu/sites/spotlight/event/portrait-warrior-conversation-artist/" class="button button--small button--flat button--default no-margin small-text">Register</a>
					</div>

				</footer>

			</article>


		</div>

		<div class="col-md--fourcol" style="padding-left: 1em;">
			<h3>Upcoming Workshops</h3>

			<ul class="list list--alternate list--nowrap">

				<li ng-repeat="workshop in workshops | orderBy:orderProp | return: 'upcoming-events' | limitTo: 5">
					<a href="{{ workshop.url }}" class="link link--undecorated">
						{{ workshop.title | clean }}
					</a> <br>
					{{ workshop.custom_fields.event_start[0] | event: 'day' }}
					{{ workshop.custom_fields.event_start_time[0] }}
				</li>

			</ul>

			<a href="http://sherman.library.nova.edu/sites/spotlight/series/academic-workshops">See all workshops</a>

		</div>

		<div class="col-md--fourcol">
			<h3>Programs and Events</h3>
			<ul class="list list--alternate list--nowrap">
				<li ng-repeat="program in programs | orderBy:orderProp | return: 'upcoming-events' | limitTo: 5">
					<a href="{{ program.url }}" class="link link--undecorated">
						{{ program.title | clean }}
					</a> <br>

					{{ program.custom_fields.event_start[0] | event: 'day' }}
					<span ng-if="program.custom_fields.event_end[0]"> &mdash; {{ program.custom_fields.event_end[0] | event: 'day' }}</span>
					<time ng-if="!program.custom_fields.event_end[0]">{{ program.custom_fields.event_start_time[0] }}</time>
				</li>
			</ul>
			<a href="http://sherman.library.nova.edu/sites/spotlight/events">See all events</a>
		</div>

	</div>

</div>

<?php
if ( isset( $_COOKIE['WelcomeMsg'] ) ):
	echo $_COOKIE['WelcomeMsg'];
else :
	echo '<span class="hide-accessible">You are logged out</span>';
endif;
?>

<?php get_footer(); ?>

<section class="modal semantic-content" id="libguides" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="false">

    <div class="modal-inner">

        <header id="modal-label">
            <h3>What are <b>Library Guides</b>?</h3>
        </header>

        <div class="modal-content epsilon clearfix">
        	<p class="no-margin">
        		Library Guides are put together by librarians about specific subjects, courses,
        		and tools, which help you navigate the library's vast resources so
        		you can find what you need or get something done a little quicker.
        	</p>
        </div>
    </div>

    <a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>

</section>
